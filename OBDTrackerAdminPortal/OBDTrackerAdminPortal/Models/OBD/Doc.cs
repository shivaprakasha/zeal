﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OBDTrackerAdminPortal.Model.OBD
{

    public class Doc
    {

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_rev")]
        public string Rev { get; set; }

        [JsonProperty("vehicalID")]
        public string VehicalID { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("RPM")]
        public string RPM { get; set; }

        [JsonProperty("speed")]
        public string Speed { get; set; }

        [JsonProperty("EngineStartTime")]
        public string EngineStartTime { get; set; }

        [JsonProperty("EngineTemperature")]
        public string EngineTemperature { get; set; }

        [JsonProperty("Fuelrate")]
        public string Fuelrate { get; set; }

        [JsonProperty("Accident")]
        public bool Accident { get; set; }

        [JsonProperty("CurrentTime")]
        public string CurrentTime { get; set; }
    }

}

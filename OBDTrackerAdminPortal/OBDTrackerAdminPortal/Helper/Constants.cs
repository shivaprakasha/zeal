﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OBDTrackerAdminPortal.Helper
{
    public static class Constants
    {
        public const string OBDBaseUrl = "https://9ba626a5-4ab2-4c2c-b0c7-cb02ef4cd816-bluemix.cloudant.com/obdtracker_zeal/";
        public const string OBDGetAllOwnerUrl ="_design/searchByType/_search/Types?q=owner"+includeDoc;
        public const string OBDGetAllVehicalsOfOwnerUrl = "_design/searchOwerVehicals/_search/OwnerVehical?q=";
        public const string OBDGetAllOBDOfVehicalUrl = "_design/searchOBDDetailsForVehical/_search/OBDDeatils?q=";
        public const string OBDSearchAccidentVehicalUrl = "_design/SearchAccidentView/_view/Accident-view?";
        public const string OBDSearchAccidentVehicalAllUrl = "_design/SearchAccidentViewAll/_view/Accident-view?q=*:*s";
        public const string includeDoc = "&include_docs=true";
        public const string includeDocNoampersand = "include_docs=true";
        public const string OBDGetAlertUrl = "_design/searchByType/_search/Types?q=Alert" + includeDoc;
    }
}
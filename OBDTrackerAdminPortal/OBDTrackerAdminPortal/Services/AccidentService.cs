﻿using OBDTrackerAdminPortal.Helper;
using OBDTrackerAdminPortal.Model.Accident;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OBDTrackerAdminPortal.Services
{
    public class AccidentService
    {
         string URL;
        public async Task<Accident> GetAllAccidentDetails()
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl+Constants.OBDSearchAccidentVehicalUrl+Constants.includeDocNoampersand;
           var accidentDetails=await objHttpService.ReadContentAsync<Accident>(URL);
           return accidentDetails;

        }
        }
    }

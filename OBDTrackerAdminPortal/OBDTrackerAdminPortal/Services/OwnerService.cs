﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OBDTrackerAdminPortal.Model;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using OBDTrackerAdminPortal.Helper;
using System.Threading.Tasks;

namespace OBDTrackerAdminPortal.Services
{

    public class OwnerService
    {
        string URL;
        public async Task<Owner> GetAllOwnerDetails()
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl+Constants.OBDGetAllOwnerUrl;
           var ownerDetails=await objHttpService.ReadContentAsync<Owner>(URL);
           return ownerDetails;

        }
        }
    }


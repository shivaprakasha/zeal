﻿using OBDTrackerAdminPortal.Helper;
using OBDTrackerAdminPortal.Model.Alert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OBDTrackerAdminPortal.Services
{
    public class AlertService
    {
        string URL;
        public async Task<Alert> GetAllAlertDetails()
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl + Constants.OBDGetAlertUrl;
           var alertDetails=await objHttpService.ReadContentAsync<Alert>(URL);
           return alertDetails;

        }
        }
    }

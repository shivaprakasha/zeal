﻿using OBDTrackerAdminPortal.Helper;
using OBDTrackerAdminPortal.Model.AccidentFullModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OBDTrackerAdminPortal.Services
{
    public class AccidentServiceAll
    {
          string URL;
        public async Task<AccidentFull> GetAllAccidentDetails()
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl+Constants.OBDSearchAccidentVehicalAllUrl+Constants.includeDoc;
           var accidentDetails=await objHttpService.ReadContentAsync<AccidentFull>(URL);
           return accidentDetails;

        }
        }
    }

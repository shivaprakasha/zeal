﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OBDTrackerAdminPortal.Helper;
using OBDTrackerAdminPortal.Model.OBD;
using System.Threading.Tasks;

namespace OBDTrackerAdminPortal.Services{

    public class VehicalOBDService
    {
        string URL;

         public async Task<VehicalOBD> GetOBDDeatilsForVehical(string VehicalID)
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl+Constants.OBDGetAllOBDOfVehicalUrl+VehicalID+Constants.includeDoc;
           var ownerDetails=await objHttpService.ReadContentAsync<VehicalOBD>(URL);
           return ownerDetails;
        }
        }
    }

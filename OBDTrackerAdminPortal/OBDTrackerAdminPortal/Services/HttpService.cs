﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace OBDTrackerAdminPortal.Services
{
    public class HttpService
    {
        public async Task<T> ReadContentAsync<T>(string url)
        {
            using (var client = new HttpClient())
            {
                Stream s = await client.GetStreamAsync(url);
              //  using (Stream s =await client.GetStreamAsync(url).Result)
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            var response = serializer.Deserialize<T>(reader);
                            return response;
                        }
                    }
                }
            }
        }
    }
}
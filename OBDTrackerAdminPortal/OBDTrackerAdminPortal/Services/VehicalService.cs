﻿using OBDTrackerAdminPortal.Helper;
using OBDTrackerAdminPortal.Model.Vehical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OBDTrackerAdminPortal.Services
{
    public class VehicalService
    {
         string URL;

         public async Task<Vehicals> GetAllVehicalsForOwner(string VehicalID)
        {
           HttpService objHttpService=new HttpService();
           URL=Constants.OBDBaseUrl+Constants.OBDGetAllVehicalsOfOwnerUrl+VehicalID+Constants.includeDoc;
           var ownerDetails=await objHttpService.ReadContentAsync<Vehicals>(URL);
           return ownerDetails;

        }
        }
    }

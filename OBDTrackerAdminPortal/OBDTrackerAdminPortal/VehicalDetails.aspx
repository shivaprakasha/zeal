﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicalDetails.aspx.cs" Inherits="OBDTrackerAdminPortal.VehicalDetails" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Zeal</title>
    <!-- Core CSS - Include with every page -->
    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/main-style.css" rel="stylesheet" />
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery.ui.all.css" type="text/css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

</head>
<body>
    <form id="Form1" runat="server">
        <!--  wrapper -->
        <div id="wrapper">
            <!-- navbar top -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
                <!-- navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <img  width="150" src="assets/img/Zeal_Logo.png" alt="" />
                    </a>
                </div>
                <!-- end navbar-header -->
                <!-- navbar-top-links -->
             <ul class="nav navbar-top-links navbar-right">
                    <!-- main dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="top-label label label-warning" runat="server" id="spnWarningCounts" ></span>  <i class="fa fa-tasks fa-3x"></i>
                        </a>
                        <!-- dropdown tasks -->
                        <ul class="dropdown-menu dropdown-tasks">
                            <asp:Repeater ID="rptWarnings" runat="server" >
                                <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong><span class="label label-warning">Warnings</span></strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="odd gradeX">
                                        <td class="fa"><%# Eval("Vehical") %>&nbsp;&nbsp(<%# Eval("BelongsTo") %>)</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><span class="label label-warning">~</span><span class="text-muted small"><%# Eval("Message1") %></span></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="label label-warning">~</span><span class="text-muted small"><%# Eval("Message2") %></span></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="label label-warning">~</span><span class="text-muted small"><%# Eval("Message3") %></span></td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ul>
                        <!-- end dropdown-tasks -->
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="top-label label label-danger" runat="server" id="spnAccidentNotification"></span><i class="fa fa-bell fa-3x"></i>
                        </a>
                        <!-- dropdown alerts-->
                        <ul class="dropdown-menu dropdown-alerts">

                            <asp:Repeater ID="rptAccidentAlert" runat="server" >
                                <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong><span class="label label-danger">Accident</span></strong>
                                                </th>
                                                <th></th>
                                                <th><asp:HyperLink  runat="server" ID="hrfViewAllAccident" NavigateUrl="~/Views/VehicalAccidentDetails.aspx">View All</asp:HyperLink></a></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="odd gradeX">
                                        <td  class="fa fa-comment fa-fw"><%# Eval("VehicalName") %>&nbsp;&nbsp(<%# Eval("VehicalBelongsTo") %>)</td>
                                        <td ><span class="text-muted small">4 mins ago
                                        </span></td>
                                        <td>
                                            <asp:Button ID="btnGetAccidentVehicalDetails" CssClass="text-muted small" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id")%>' OnClick="btnShowModal_Click" Text="View" runat="server" />
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ul>
                        <!-- end dropdown-alerts -->
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-3x"></i>
                        </a>
                        <!-- dropdown user-->
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i>User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i>Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="Views/Login.aspx"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                            </li>
                        </ul>
                        <!-- end dropdown-user -->
                    </li>
                    <!-- end main dropdown -->
                </ul>
                <!-- end navbar-top-links -->

            </nav>
            <!-- end navbar top -->

            <!-- navbar side -->
            <nav class="navbar-default navbar-static-side" role="navigation">
                <!-- sidebar-collapse -->
                <div class="sidebar-collapse">
                    <!-- side-menu -->
                    <ul class="nav" id="side-menu">
                        <li>
                            <!-- user image section-->
                            <div class="user-section">
                                <div class="user-section-inner">
                                    <img src="assets/img/user.jpg" alt="">
                                </div>
                                <div class="user-info">
                                    <div>SHIVA <strong>PRAKASH</strong></div>
                                    <div class="user-text-online">
                                        <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                    </div>
                                </div>
                            </div>
                            <!--end user image section-->
                        </li>
                        <li class="sidebar-search">
                            <!-- search section-->
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!--end search section-->
                        </li>
                        <li class="selected">
                            <a href="DashBoard.aspx"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li class="">
                            <a href="Views/VehicalAccidentDetails.aspx"><i class="fa fa-table fa-fw"></i>Accident</a>
                        </li>
                        <li>
                            <a href="#" id="NotImplemented"><i class="fa fa-bar-chart-o fa-fw"></i>Charts<span class="fa arrow"></span></a>
                        </li>
                    </ul>
                    <!-- end side-menu -->
                </div>
                <!-- end sidebar-collapse -->
            </nav>
            <!-- end navbar side -->
            <!--  page-wrapper -->
            <div id="page-wrapper">


                <div class="row">
                    <!--  page header -->
                    <div class="col-lg-12">
                        <h1 class="page-header">Vehical</h1>
                    </div>
                    <!-- end  page header -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Advanced Tables -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Vehical Information
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <asp:Repeater ID="rptVehicalsList" runat="server" OnItemDataBound="rptVehicalsList_ItemCommand">
                                        <HeaderTemplate>
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Vehical</th>
                                                        <th>Type</th>
                                                        <th>Vehical Type</th>
                                                        <th>Belongs to</th>
                                                        <th></th>

                                                    </tr>
                                                    <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="odd gradeX">
                                                <td><%# Eval("VehicalName") %></td>
                                                <td><%# Eval("Vehical") %></td>
                                                <td><%# Eval("VehicalType") %></td>
                                                <td class="center"><%# Eval("VehicalBelongsTo") %></td>
                                                <td>
                                                    <asp:Button ID="btnGetOBDDetails" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id")%>' Text="View Details" runat="server" />
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
    </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>

                            </div>
                        </div>
                        <!--End Advanced Tables -->
                    </div>
                </div>

            </div>
            <!-- end page-wrapper -->

        </div>
                <div  title="Basic modal dialog" style="display:none;" id="popupdiv"  class="panel-body"   >
                 <table class="table">
                          <tbody>
                                        <tr>
                                            <td style="text-align-last:end">Vehical Identity</td>
                                            <td style="text-align-last:center">:</td>
                                            <td style="text-align-last:left"><asp:Label ID="lblVehicalID" runat="server" Text=""></asp:Label></td>
                                       
                                        </tr>
                                        <tr>
                                           <td style="text-align-last:end">Vehical</td>
                                            <td style="text-align-last:center">:</td>
                                            <td style="text-align-last:left"><asp:Label ID="lblVehical" runat="server" Text=""></asp:Label></td>
                                       
                                        </tr>
                                        <tr>
                                            <td style="text-align-last:end">Vehical Belongs To</td>
                                            <td style="text-align-last:center">:</td>
                                            <td style="text-align-last:left"><asp:Label ID="lblVehicalBelingsTO" runat="server" Text=""></asp:Label></td>
                                       
                                        </tr>
                               <tr>
                                            <td style="text-align-last:end">Vehical Type</td>
                                            <td style="text-align-last:center">:</td>
                                            <td style="text-align-last:left"><asp:Label ID="lblVehicaltype" runat="server" Text=""></asp:Label></td>
                                       
                                        </tr>
                               <tr>
                                            <td style="text-align-last:end">Accident Time</td>
                                            <td style="text-align-last:center">:</td>
                                            <td style="text-align-last:left"><asp:Label ID="lblAccidentTime" runat="server" Text=""></asp:Label></td>
                                       
                                        </tr>
                                    </tbody>
                                </table>

</div>

        <!-- end wrapper -->

        <!-- Core Scripts - Include with every page -->
        <script src="assets/plugins/jquery-1.10.2.js"></script>
        <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="assets/plugins/pace/pace.js"></script>
        <script src="assets/scripts/siminta.js"></script>
        <!-- Page-Level Plugin Scripts-->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
         <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();

                $('#NotImplemented').click(function (event) {
                    var mytext = "This page Not Implemented!";

                    $('<div id="dialog"  style="text-align:center; vertical-align: middle;" >' + mytext + '</div>').appendTo('body');
                    event.preventDefault();

                    $("#dialog").dialog({
                        title: "Not Implemented",
                        width: 400,
                        modal: true,
                        open: function () {
                            $('.ui-widget-overlay').css('background', 'green');
                        },
                        close: function (event, ui) {
                            $("#dialog").hide();
                        }
                    });
                });
            });


            function showmodalpopup() {
                $("#popupdiv").dialog({
                    title: "Accident Information",
                    width: 500,
                    height: 450,
                    modal: true,
                    open: function () {
                        $('.ui-widget-overlay').css('background', 'green');
                    },
                    close: function () {
                        $('.ui-widget-overlay').css('background', 'white');
                    },
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            };

        </script>
    </form>
</body>

</html>


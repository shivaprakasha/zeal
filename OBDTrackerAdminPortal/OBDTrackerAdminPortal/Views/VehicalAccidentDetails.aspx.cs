﻿using OBDTrackerAdminPortal.Model;
using OBDTrackerAdminPortal.Model.Accident;
using OBDTrackerAdminPortal.Model.AccidentFullModel;
using OBDTrackerAdminPortal.Model.Alert;
using OBDTrackerAdminPortal.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OBDTrackerAdminPortal.Views
{
    public partial class VehicalAccidentDetails : System.Web.UI.Page
    {
        AccidentFull allAccident;
        protected void Page_Load(object sender, EventArgs e)
        {
            allAccident = new AccidentFull();
            if (!IsPostBack)
            {
              
            }
            BindAccidentVehicalDetails();
            BindAlertDetails();

        }

        private async void BindAccidentVehicalDetails()
        {
            AccidentServiceAll accident = new AccidentServiceAll();
            allAccident = await accident.GetAllAccidentDetails();
            if (allAccident != null)
            {
                var Accident =( from Acc in allAccident.Rows where Acc.Value!=null select Acc.Doc).ToList();

                allAccident.OwnerList = (from Acc in allAccident.Rows where Acc.Value == null select Acc.Doc).ToList();

                rptAccidentDetails.DataSource = Accident;
                rptAccidentDetails.DataBind();
            }

        }
        private async void BindAlertDetails()
        {
            AlertService alert = new AlertService();
            Alert allAlerts = await alert.GetAllAlertDetails();
            if (allAlerts != null)
            {
                var Accident = allAlerts.Rows.Select(Alrt => Alrt.Doc).ToList();
                rptWarnings.DataSource = Accident;
                rptWarnings.DataBind();
                spnWarningCounts.InnerText = allAlerts.TotalRows.ToString();
            }
        }

        protected void rptAccidentDetails_ItemCommand(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var vehicalOwnerID = e.Item.FindControl("hdnOwnerID") as HiddenField;
                if (vehicalOwnerID != null)
                {
                    var OwnerDetails = allAccident.OwnerList.SingleOrDefault(ownr => ownr.Id == vehicalOwnerID.Value);
                    var OwnerName = e.Item.FindControl("lblOwner") as Label;
                    var Email = e.Item.FindControl("lblEmail") as Label;
                    var PhoneNo = e.Item.FindControl("lblPhoneNo") as Label;
                    OwnerName.Text = OwnerDetails.CompanyName;
                    Email.Text = OwnerDetails.EmailId;
                    PhoneNo.Text = OwnerDetails.PhoneNumber;                   
                }
            }
        }

        protected void imgBtnShowMap_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "showmodalpopup();", true);
        }       
    }
}
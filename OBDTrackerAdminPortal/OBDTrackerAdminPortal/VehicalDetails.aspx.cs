﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OBDTrackerAdminPortal.Services;
using OBDTrackerAdminPortal.Model.Vehical;
using OBDTrackerAdminPortal.Model.Accident;
using OBDTrackerAdminPortal.Model.Alert;


namespace OBDTrackerAdminPortal
{
    public partial class VehicalDetails : System.Web.UI.Page
    {
        Accident allAccident;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                allAccident = new Accident();
                bindVehicals();
                BindAccidentVehicalDetails();
                BindAlertDetails();
            }
        }

        private async void bindVehicals()
        {
            VehicalService vehicals = new VehicalService();
            string vehicalID = Request.QueryString["VehicalOwnerID"] ;
            Vehicals allVehicals = await vehicals.GetAllVehicalsForOwner(vehicalID);
            if (allVehicals != null)
            {
                var ownerList = allVehicals.Rows.Select(ownr => ownr.Doc).ToList();
                rptVehicalsList.DataSource = ownerList;
                rptVehicalsList.DataBind();
            }
        }
        private async void BindAccidentVehicalDetails()
        {
            AccidentService accident = new AccidentService();
            allAccident = await accident.GetAllAccidentDetails();
            if (allAccident != null)
            {
                var AccidentInfos = allAccident.Rows.Select(Acc => Acc.Doc).ToList();
                ViewState.Add("AccidentInfo", AccidentInfos);
                rptAccidentAlert.DataSource = AccidentInfos;
                rptAccidentAlert.DataBind();
                spnAccidentNotification.InnerText = allAccident.TotalRows.ToString();
            }

        }
        private async void BindAlertDetails()
        {
            AlertService alert = new AlertService();
            Alert allAlerts = await alert.GetAllAlertDetails();
            if (allAlerts != null)
            {
                var Accident = allAlerts.Rows.Select(Alrt => Alrt.Doc).ToList();
                rptWarnings.DataSource = Accident;
                rptWarnings.DataBind();
                spnWarningCounts.InnerText = allAlerts.TotalRows.ToString();
            }
        }

        protected void rptVehicalsList_ItemCommand(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var btnVehicalDetails = e.Item.FindControl("btnGetOBDDetails") as Button;
                if (btnVehicalDetails != null)
                {
                    btnVehicalDetails.PostBackUrl = "VehicalOBDTrackerDetails.aspx?VehicalID=" + btnVehicalDetails.CommandArgument.ToString();
                }
            }
        }
        protected void btnShowModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Show Modal Popup", "showmodalpopup();", true);

            Button _myButton = (Button)sender;
            string VehicalId = _myButton.CommandArgument.ToString();
            var AccidentDetails = (List<Model.Accident.Doc>)(this.ViewState["AccidentInfo"]);

            var Accident = AccidentDetails.SingleOrDefault(acc => acc.Id == VehicalId);
            lblVehicalID.Text = Accident.Id;
            lblVehical.Text = Accident.Vehical;
            lblVehicalBelingsTO.Text = Accident.VehicalBelongsTo;
            lblVehicaltype.Text = Accident.VehicalType;
            lblAccidentTime.Text = DateTime.Now.AddMinutes(-4).ToShortTimeString().ToString();
        }
    }
}